90023901342

# Ссылки

## DDD (Предметно-ориентированное проектирование)
- [Глоссарий](https://gitlab.com/soccoop-platform/site/product/-/blob/main/docs/glossary.md) - набор терминов, описывающих процессы операционки
- [Карта контекстов в Figma](https://www.figma.com/file/Hs5dwudHPyM0fCHrMCyUHX/SocCoop-Context-Map?type=whiteboard&node-id=0-1) - карта контекстов (DDD), визуализирующая сценарии операционки
