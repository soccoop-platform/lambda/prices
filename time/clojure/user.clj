(ns user
  (:require
    [clj-commons.pretty.repl :refer [install-pretty-exceptions]]
    [clojure.tools.namespace.repl :refer [refresh]]))

(install-pretty-exceptions)
