(import
  (guix packages)
  (gnu packages clojure)
  (gnu packages java)
  (gnu packages haskell-apps)
  ; our temporary packages, just to assist development, some will go to `team`
  ; SEE: share/guix/packages.scm
  (packages))


(packages->manifest
  (list
    openjdk17
    clojure-tools   ; nowadays it is "enough" for development environment
    shellcheck
    lambda-webroot  ; will pull everything needed for "weby" things
    coreui))        ; UI library
