(ns red.prices.http.api-test
  (:require
   [clojure.test :refer [run-tests]]
   [expectations.clojure.test
     :refer [defexpect expect expecting]]
   [red.prices.http.api :refer :all]
   [ring.mock.request :as mock]))


(defexpect handlers
  (expecting "Example HTTP API tests written WITHOUT any \"server\" involvement"
    (expect
      ; `hello` is a "handler" - pass ring request, get ring response!
      (hello
        ; `mock/request` is "request map builder" (especially handy for tests)
        (mock/request :get "/"))
      ; In Clojure, those "weird" request/responses are JUST maps (ring maps, to be specific).
      ; SEE: https://practical.li/clojure-web-services/reference/ring/
      {
        :status 200
        :headers
        {
          "Content-Type" "text/plain;charset=UTF-8"
          "Vary" "Accept"}
        :body "Hello, the app is under development."})
    (expect
      (hello (mock/header
               (mock/request :get "/")
               "Accept" "application/edn"))
      {
        :status 200
        :headers
        {
          "Content-Type" "application/edn;charset=UTF-8"
          "Vary" "Accept"}
        :body "Hello, the app is under development."})))


; Short-cut to run tests from editor: uncomment & eval (and see output in terminal)!
; (run-tests)
