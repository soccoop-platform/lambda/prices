(ns red.prices.web-test
  (:require
    [clojure.test :refer :all]
    [kerodon.core :refer :all]
    [kerodon.test :refer :all]
    [red.prices.web :refer [app]]
    [clojure.java.io :as io]))


(deftest root-test
  (testing "Root page does not exist"
    (-> (session app)
        (visit "/")
        (has (status? 404)
             (text? "Oops!")))))  ; todo


(deftest static-assets-test
  (testing "Static assets served"
    (-> (session app)
        (visit "/static/js/coreui.esm.min.js")
        (has (status? 200))
        (visit "/static/css/coreui.min.css")
        (has (status? 200)))))


(deftest prices-update-test
  (testing "*Responsible for categories* updates *Prices* for retailer"
    (-> (session app)
        (visit "/prices/update")
        (has (status? 200))
        (attach-file "Лист поставщика:" (io/file "test/fixtures/Закупка-14.11-Окей.csv"))
        (press "Обновить цены!")
        (has (status? 201))
        (follow-redirect))))

; Short-cut to run tests from editor: uncomment & eval (and see output in terminal)!
; (run-tests)
