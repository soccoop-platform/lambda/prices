(ns red.prices.cli
  (:require
    [docopt.core :refer [docopt]]
    [org.httpkit.server :as http]
    ; ours
    ; @denis See?! A pinch of *meaningful* naming - and it *speaks for itself*, without going into detail
    ; denis: Ok, I see.
    [red.prices.web :refer [app]])
  (:gen-class))


(def MAN "Retailer Prices.

Usage:
  prices-app <command> [options...]

Commands:
  web    Serve application over Web.")

; Web options:
;   -l ADDR, --listen=<addr>    Listen on specified address, can be passed multiple times.
;   -p PORT, --port=<port>      Bind to specified port instead of random.")


(defn -main [& args]
  (when-let [argm (docopt MAN args)]
    (clojure.pprint/pprint argm)
    (case (argm "<command>")
      "web" (http/run-server app)

      (println "Unknown sub-command"))))
