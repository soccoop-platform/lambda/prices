(ns red.prices.web
  (:require
    [compojure.core :refer [defroutes GET POST]]
    [compojure.route :as route]
    [huff.core :as html]
    [liberator.core :refer [defresource]]
    [ring.middleware.keyword-params :refer [wrap-keyword-params]]
    [ring.middleware.params :refer [wrap-params]]
    ; ours
    [red.prices.core :refer [retailer-pricelist->urls]]
    [red.prices.parser :as parser]))


(defn update-prices-page []
  (html/page
    [:html
      [:head
        [:meta {:charset "utf-8"}]
        [:meta {:name "viewport" :content "width=device-width, initial-scale=1"}]
        [:link {:href "/static/css/coreui.min.css" :rel "stylesheet"}]
        [:title {:lang "ru"} "Обновить розничные цены"]]
      [:body
        [:div.container.text-center
          [:div.row
            [:div.col
              [:h1 "Обновление розничных цен для поставщика"]
              ; to/do: Retailer pricelist uploading
              [:input#retailer-prices]]]]]]))


(defresource prices-update
  :available-media-types ["text/html"]
  :handle-ok (update-prices-page)
  :post! (fn [ctx]
            (let [urls (retailer-pricelist->urls (get-in ctx :request :params :retailer-prices))]
              ;; OPTIMIZE: Use cache with previously parsed values instead of immediate sequential request to the pages, that takes a lot of time.
              (parser/parse-pages urls))))


(defroutes app-routes
  (GET "/prices/update" [] prices-update)
  (route/resources "/static" {:root "static"})
  (route/not-found "Oops!"))


(def app
  (-> app-routes
      (wrap-params)
      (wrap-keyword-params)))
