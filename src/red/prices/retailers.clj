(ns red.prices.retailers
  (:require
   [clojure.string :as str]
   [odysseus.xpath :as xpath]
   [red.prices.product :refer [make-product]]))

(defn- detect-retailer-by-url [opened-page]
  (cond
    (re-seq #"okeydostavka" (:url opened-page)) :okey
    :default nil))

(defmulti parse-page detect-retailer-by-url)

(defmethod parse-page :okey [opened-page]
  (let [{:keys [url html] :or {url nil}} opened-page
        title (xpath/select-xpath html "//h1[@role='heading']/text()")
        price (xpath/select-xpath html "//meta[@itemprop='price']/@content")
        weight (try
                  (as->
                    html
                    $
                    (xpath/select-xpath $ "//li[@class='breadcrumb-page__item breadcrumb-page__current']/text()")
                    (re-find #" \d*(?:кг|г|л)\b" $)
                    (and $ (str/trim $)))
                  (catch Exception _ nil))]
    (make-product url title price weight)))
