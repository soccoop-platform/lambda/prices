(ns red.prices.parser
  (:require
    [edn-config.core :refer [read-config]]
    [etaoin.api :as e]
    [red.prices.retailers :as retailers]))

(defn- get-html [driver url]
  (e/go driver url)
  (e/get-source driver))

(defn parse-pages [urls]
  (e/with-firefox
    (-> "resources/config.edn" read-config :webdriver :opts)
    driver
    (reduce
      (fn [products url]
        (conj
          products
          (retailers/parse-page
            {:url url :html (get-html driver url)})))
      []
      urls)))
