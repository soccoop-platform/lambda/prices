(ns red.prices.product
  (:require
    [clojure.spec.alpha :as s]
    [spec-dict :refer :all]))

;; Let's use WHATWG URL spec (https://www.evanlouie.com/gists/clojure/src/com/evanlouie/net/url.cljc)
(defn url? [s]
  (re-matches #"^([a-zA-Z]+:)//(([^:]+)(:(.+))?@)?(([a-zA-Z0-9][a-zA-Z0-9-]{0,61}[a-zA-Z0-9]\.)+)([a-zA-Z]{2,63})(:(\d+))?(/([^?#]+)?(\?([^#]+)?)?(#(.+)?)?)?$") s)

(s/def ::nestring (s/and string? not-empty))

(s/def ::product
  {:url (s/nilable url?)
   :hash int?
   :title ::nestring
   :price (s/nilable string?)
   :weight (s/nilable string?)})

(defn make-product [url title price weight]
  {
    :url url
    :hash (hash (str title price weight))
    :title title
    :price price
    :weight weight})
