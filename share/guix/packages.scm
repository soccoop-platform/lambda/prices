(library (packages)
  (export lambda-webroot coreui) ; @popperjs/core)
  (import
    (scheme base)
    (prefix (guix licenses) license:)
    (guix build-system copy)
    (guix build-system trivial)
    (guix download)
    (guix gexp)
    (guix git-download)
    (guix packages)
    (guix search-paths)))


(define lambda-webroot
  (package
    (name "lambda-webroot")
    (version "0")
    (source #f)
    (build-system trivial-build-system)
    (arguments
      (list
        #:modules '((guix build utils))
        ; dummy builder bucause a package must have one
        #:builder
        #~(begin
            (use-modules
              (guix build utils))

            (mkdir-p (string-append (assoc-ref %outputs "out") "/srv/www/static")))))
    (native-search-paths
      (list
        (search-path-specification
          (variable "LAMBDA_WEBROOT_STATIC_PATH")
          (files (list "srv/www/static")))))
    (synopsis "Dummy package to just only define Search Path so Guix will pick it up")
    (description #f)
    (license license:wtfpl2)
    (home-page #f)))


(define coreui
  (package
    (name "coreui")
    (version "4.3.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/coreui/coreui")
              (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "16cj6glpvfpm32zphqqaq7f182v970bvv47b4iplfl1x3c11mqp2"))))
    (build-system copy-build-system)
    (arguments
      (list
        #:install-plan
        #~'(("dist/" "/srv/www/static/"
             #:include ("css/coreui.min.css" "js/coreui.esm.min.js")))))
    (synopsis
      "Open Source UI Kit built on top of Bootstrap 5 and plain JavaScript without
any additional libraries like jQuery")
    (description
      "Fastest way to build a modern dashboard for any platforms, browser, or device.

A complete UI Kit that allows you to quickly build eye-catching, high-quality,
high-performance responsive applications.")
    (license license:expat)
    (home-page "https://coreui.io")))
