# Среда разработки

- [установить Guix](https://guix.gnu.org/en/manual/en/html_node/Installation.html)
- [установить `direnv`](https://direnv.net/docs/installation.html) (можно таки сразу через guix)

```console
$ cp .envrc{.sample,}
$ direnv allow
$ ln -vsr time/to do
```

## Запуск тестов

```console
$ do/test
```

Выполнятся тесты, находящиеся в `test/*` _И_ имена файлов оканчиваются на `*_test.clj` (и соотвествующим образом назван неймспейс Clojure).

## Запуск "приложения"

```console
$ do/app
```

## А как зайти в REPL?

```console
$ do/repl
```

## А как подключиться к REPL'у из редактора?

- https://nrepl.org/nrepl/usage/clients.html#editorsides
